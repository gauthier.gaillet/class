# Préjury 22 décembre 2023 de 10h à 14h

## Présentations PDF

Déposez vos présentations pdf (16 slides) dans ce dossier et intulez-les de la manière suivante : "group-00.pdf" (en adaptant l'intitulé à votre numéro de groupe).

* [group-01](./group-01.pdf)
* [group-02](./group-02.pdf)
* [group-03](./group-03.pdf)
* [group-04](./group-04.pdf)
* [group-05](./group-05.pdf)
* [group-06](./group-06.pdf)
* [group-07](./group-07.pdf)
* [group-08](./group-08.pdf)
* [group-09](./group-09.pdf)
