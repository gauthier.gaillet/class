# Group Meetings

Voici quelques plages horaires ouvertes pour faire le point sur votre projet avec moi. Inscrivez votre groupe ci-dessous. J'aimerais voir chaque groupe au moins une fois soit le 21/11, le 27/11 ou le 28/11.  

### Ma 21/11/23

12h00 à 12h30 :  
12h30 à 13h00 : [Group 5](https://group-05-fablab-ulb-enseignements-2023-2024-fabz-39030c60f8d6dc.gitlab.io/)  
13h00 à 13h30 : [Group 3](https://group-03-fablab-ulb-enseignements-2023-2024-fabz-ee48e58995de77.gitlab.io/)  
13h30 à 14h00 : [Group 6](https://group-06-fablab-ulb-enseignements-2023-2024-fabz-a6f2ed73c1fcfa.gitlab.io/)  

### Lu 27/11/23

12h30 à 13h00 : [Group 9](https://group-09-fablab-ulb-enseignements-2023-2024-fabz-d9e3d9832514ca.gitlab.io/)  
13h00 à 13h30 : [Group 4](https://group-04-fablab-ulb-enseignements-2023-2024-fabz-7ec48573c41854.gitlab.io/)  
13h30 à 14h00 :  

### Ma 28/11/23

12h30 à 13h00 :  [Group 1](https://group-01-fablab-ulb-enseignements-2023-2024-fabz-61274de117f212.gitlab.io/)  
13h00 à 13h30 :   [Group 2](https://group-02-fablab-ulb-enseignements-2023-2024-fabz-4729e3c9136aa4.gitlab.io/)  
13h30 à 14h00 :  [Group 8](https://group-08-fablab-ulb-enseignements-2023-2024-fabz-2130c25e31684d.gitlab.io/)   
14h00 à 14h30 : [Group 7](https://group-07-fablab-ulb-enseignements-2023-2024-fabz-4315fb17383891.gitlab.io/) 


### Lu 11/12/23

10h24 à 10h48 :  groupe 3

10h48 à 11h12 :  

11h12 à 11h36 : groupe 4 

11h36 à 12h24 :  

12h24 à 12h48 :  [Group 1](https://group-01-fablab-ulb-enseignements-2023-2024-fabz-61274de117f212.gitlab.io/)

12h48 à 13h12 :  groupe6

13h12 à 13h36 :  [Group 5](https://group-05-fablab-ulb-enseignements-2023-2024-fabz-39030c60f8d6dc.gitlab.io/)  

13h36 à 14h00 :  groupe 2

14h00 à 14h24 :  groupe 8

### Ma 19/12/23 (si impossible le 11/12/23)

12h00 à 12h30 :  
12h30 à 13h00 :  groupe 9
13h00 à 13h30 : groupe7 
13h30 à 14h00 :   
